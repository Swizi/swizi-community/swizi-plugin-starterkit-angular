# React-Redux Starter Kit for Swizi Plugin

This starter-kit is a project you can fork to start a new Angular plugin for Swizi.
It contains base elements for view rendering and asynchronous actions.

this starter kit is built to work with our [Chrome extention](https://chrome.google.com/webstore/detail/swizi-plugin-dev-helper/kafcdkbaiijoccgiefljmlanpabnigao). It will emulate the behaviour of the swizi sdk when running on device. We recommend you to use it, but if you don't want, read [this part](#wait-for-gamoready-event) so you can run your plugin on computer.

## Table of Contents

* [Setup](#setup)
* [How to use starter kit](#how-to-use-starter-kit)
  * [Wait for gamoReady event](#wait-for-gamoready-event)
  * [Build your plugin](#build-your-plugin)

---

## Setup

This project use the [Angular CLI](https://cli.angular.io/). 

1. Clone this project
2. Install depedencies

        npm install

3. Run dev server

        ng serve --open

4. Your navigator should know open a web page with a form, allowing you to browse git repository.

## How to use starter kit

### Wait for gamoReady event

`src/app/app.component.ts`

```typescript
swiziReady:boolean = false

ngOnInit() {
  document.addEventListener('gamoReady', e => {
    this.swiziReady = true
  })
}
```
`src/app/app.component.html`

```html
<div *ngIf="swiziReady">
    ...your app
</div>
```
`src/index.html`

```html
<!DOCTYPE html>
<html lang="fr" data-swizi="true">
  ...
</html>
```

To be sure the swizi sdk is available when working with the [Chrome extention](https://chrome.google.com/webstore/detail/swizi-plugin-dev-helper/kafcdkbaiijoccgiefljmlanpabnigao), you have to wait for the `gamoReady` event to be fired. This event is fired when running your plugin on device and with the extension.

you also have to add the `data-swizi="true"` property to the html element to let the extension know that it have to be injected into your app.

These lines are not mandatory. But you **MUST** delete them if you don't want to work with the extension. Or your component will never be rendered, as it is the extension that fire the gamoReady event.

### Build your plugin

To build your application just launch :

        ng build