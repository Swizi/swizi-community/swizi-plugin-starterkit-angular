import { Component, OnInit } from "@angular/core";
import * as swizi from "swizi";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.less"],
})
export class HeaderComponent implements OnInit {
  device: string = "";
  version: string = "";

  constructor() {}

  ngOnInit() {
    swizi.getPlatform().then(d => {
      swizi.getPlatformVersion().then(v => {
        this.device = d

        if (v.major)
          this.version = v.major + "." + v.minor + "." + v.patchVersion;
        else if (v.sdk_int)
          this.version =
            v.sdk_int + "." + v.security_patch + "." + v.release;
      });
    });
  }
}
