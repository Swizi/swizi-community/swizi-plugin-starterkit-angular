import { Component, OnInit } from "@angular/core";
import { GithubService } from "../../Services/github.service";
import * as swizi from "swizi";

@Component({
  selector: "app-form",
  providers: [GithubService],
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.less"],
})
export class FormComponent implements OnInit {
  repos: object = {};
  projectName: string = "pong";
  language: string = "java";
  loading: boolean = false;

  constructor(private githubService: GithubService) {}

  _getrepos(): void {
    this.loading = true;
    this.githubService
      ._fetchRepo(this.projectName, this.language)
      .then(repotab => {
        swizi.log("log", "Fetched " + repotab.total_count + " git repos");
        this.repos = repotab;
        this.loading = false;
      });
  }

  ngOnInit() {
    this._getrepos();
  }
}
