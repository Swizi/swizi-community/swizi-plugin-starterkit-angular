import { Component, Input } from "@angular/core";

@Component({
  selector: "app-card-list",
  templateUrl: "./card-list.component.html",
  styleUrls: ["./card-list.component.less"],
})
export class CardListComponent {
  @Input() repos: object;
  @Input() loading: boolean;
}
