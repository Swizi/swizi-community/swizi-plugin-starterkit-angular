import { Injectable } from "@angular/core";

@Injectable()
export class GithubService {
  _fetchRepo(name, lang) {
    return fetch(
      "https://api.github.com/search/repositories?q=" +
        name +
        "+language:" +
        lang +
        "&sort=stars&order=desc",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      },
    )
      .then(r => r.json())
  }
}
