import { Component, AfterViewInit } from "@angular/core";
import * as swizi from "swizi";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styles: [],
})
export class AppComponent implements AfterViewInit {
  swiziReady: boolean = false;

  ngAfterViewInit() {
    document.addEventListener("swiziReady", e => {
      console.log(swizi);
      swizi.log("log", "Swizi has been injected in app");
      this.swiziReady = true;
    });
  }
}
